// Cache limit is 12 hrs
// h * m * s * ms

const HOUR_LIMIT = 12;
const MINUTES_IN_AN_HOUR = 60;
const SECONDS_IN_A_MINUTE = 60;
const MS_IN_A_SECOND = 1000;

export default HOUR_LIMIT * MINUTES_IN_AN_HOUR * SECONDS_IN_A_MINUTE * MS_IN_A_SECOND;
