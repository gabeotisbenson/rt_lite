const DUMMY_PROTOCOL = 'https://';
const DUMMY_DOMAIN = 'gabe.xyz';

export const isValidUrl = url => {
	if (!url) return false;

	let standardizedUrl = url;

	if (standardizedUrl.indexOf('/') === 0) standardizedUrl = DUMMY_PROTOCOL + DUMMY_DOMAIN + standardizedUrl;

	try {
		new URL(url);
	} catch (err) {
		try {
			new URL(`http://${url}`);
		} catch (err) {
			return false;
		}
	}

	return true;
};
