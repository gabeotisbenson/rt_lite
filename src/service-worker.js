import { NetworkFirst } from 'workbox-strategies';
import { registerRoute } from 'workbox-routing';

const routes = {
	index: /\/$/u,
	sameHost: /\/*/u,
	anyHost: /.+\/*/u
};

for (const key in routes) registerRoute(
	routes[key],
	new NetworkFirst({ cacheName: 'dynamic' })
);
